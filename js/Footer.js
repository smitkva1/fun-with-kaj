/**
 * Zapati aplikace
 * Nachazi se v hierarchii pod tridou App
 */
class Footer extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <footer>
        <span className="footer-text">
          Toto webová aplikace byla vytovořena studentem SITu pro předmět KAJ
        </span>
      </footer>
    );
  }
}

/**
 * Trida zajistujici prekvapeni => mini svg hru
 * Nachazi se bokem od Reactu
 */
class EasterEgg {
  constructor() {
    //klicove promene
    this.have = 0;
    this.max = 5;
    this.time = 10;
    this.show = 1000;
    this.peopleIndex = 0;
    this.timeNode;
    this.scoreNode;
    const hills = [
      { x: 10, y: 90 },
      { x: 0, y: 250 },
      { x: 100, y: 260 },
      { x: 260, y: 250 },
      { x: 180, y: 180 },
      { x: 130, y: 100 },
      { x: 70, y: 160 },
      { x: 240, y: 90 },
    ];
    const people = [
      { x: 15, y: 70 },
      { x: 170, y: 240 },
      { x: 345, y: 230 },
      { x: 40, y: 150 },
      { x: 330, y: 70 },
    ];

    //korenove svg
    const bridge = document.querySelector("body");
    this.svgNS = "http://www.w3.org/2000/svg";
    this.svg = document.createElementNS(this.svgNS, "svg");
    this.svg.setAttributeNS(null, "viewBox", "0 0 300 300");
    this.svg.setAttributeNS(null, "class", "easterEgg");
    bridge.appendChild(this.svg);

    //tvorba prostredi
    this.makeCloseIcon();
    this.drawCountrySide(hills);
    this.createInfoText("Zkuz mě najít!", -50, 120);
    this.createInfoText("Click!", 280, 120);
    this.showScore(this.have, this.max);

    this.playGame(people);
  }

  /**
   * Metoda zajistu zavreni okna
   */
  makeCloseIcon() {
    const group = document.createElementNS(this.svgNS, "g");
    this.svg.appendChild(group);
    const p1 = document.createElementNS(this.svgNS, "path");
    p1.setAttributeNS(null, "d", "M340 10 L350 20");
    p1.setAttributeNS(null, "fill", "transparent");
    p1.setAttributeNS(null, "stroke", "black");
    group.appendChild(p1);

    const p2 = document.createElementNS(this.svgNS, "path");
    p2.setAttributeNS(null, "d", "M350 10 L340 20");
    p2.setAttributeNS(null, "fill", "transparent");
    p2.setAttributeNS(null, "stroke", "black");
    group.appendChild(p2);

    group.classList.add("clickable");
    group.addEventListener("click", e => {
      let reactRoot = document.querySelector("#root");
      reactRoot.classList.remove("onBackSide");
      this.svg.remove();
    });
  }

  /**
   * Metoda vzkreslujici hory
   * @param {*} hills souradnice kopcu
   */
  drawCountrySide(hills) {
    hills.forEach(elem => {
      this.drawHill(elem.x, elem.y);
    });
  }

  /**
   * Metoda vykreslujici kopec
   * @param {*} x x-ova souradnice
   * @param {*} y y-ova souradnice
   */
  drawHill(x, y) {
    const hill = document.createElementNS(this.svgNS, "path");
    hill.setAttributeNS(
      null,
      "d",
      `M ${x},${y} c 20,0 15,-80 40,-80 s 20,80 40,80`,
    );
    hill.setAttributeNS(null, "fill", "green");
    hill.setAttributeNS(null, "stroke", "black");
    hill.setAttributeNS(null, "stroke-width", "2");
    this.svg.appendChild(hill);
  }

  /**
   * Metoda vykreslujici text
   * @param {*} info text infa
   * @param {*} x x-ova souradnice
   * @param {*} y y-ova souradnice
   */
  createInfoText(info, x, y) {
    const text = document.createElementNS(this.svgNS, "text");
    text.setAttributeNS(null, "x", x);
    text.setAttributeNS(null, "y", y);
    let txt = document.createTextNode(info);
    text.appendChild(txt);
    this.svg.appendChild(text);
  }

  /**
   * Metoda vykreslujici text skore
   * @param {*} have kolik hrac nasel
   * @param {*} max kolik muze najit celkem
   */
  showScore(have, max) {
    if (this.scoreNode) {
      this.scoreNode.remove();
    }
    this.scoreNode = document.createElementNS(this.svgNS, "text");
    this.scoreNode.setAttributeNS(null, "x", "-50");
    this.scoreNode.setAttributeNS(null, "y", "140");
    let txt = document.createTextNode(have + "/" + max);
    this.scoreNode.appendChild(txt);
    this.svg.appendChild(this.scoreNode);
  }

  /**
   * Metoda zobrazujici jako cas jako text
   * @param {*} time zbyvajici cas
   */
  showTime(time) {
    let timeText = "0:";
    if (this.timeNode) {
      this.timeNode.remove();
    }
    if (time > 9) {
      timeText += time;
    } else {
      timeText += "0" + time;
    }

    this.timeNode = document.createElementNS(this.svgNS, "text");
    this.timeNode.setAttributeNS(null, "x", "10");
    this.timeNode.setAttributeNS(null, "y", "140");
    let txt = document.createTextNode(timeText);
    this.timeNode.appendChild(txt);
    this.svg.appendChild(this.timeNode);
  }

  /**
   * Metoda zobrazujici hledany objekt
   * @param {*} x x-ova souradnice
   * @param {*} y y-ova souradnice
   * @param {*} showTime cas jak dlouho bude videt
   */
  showMe(x, y, showTime) {
    const group = document.createElementNS(this.svgNS, "g");
    group.classList.add("clickable");
    this.svg.appendChild(group);

    const head = document.createElementNS(this.svgNS, "circle");
    head.setAttributeNS(null, "cx", x);
    head.setAttributeNS(null, "cy", y);
    head.setAttributeNS(null, "r", "5");
    head.setAttributeNS(null, "fill", "yellow");
    head.setAttributeNS(null, "stroke", "black");
    head.setAttributeNS(null, "stroke-width", "2");
    group.appendChild(head);

    const li1 = document.createElementNS(this.svgNS, "path");
    li1.setAttributeNS(null, "d", `M${x} ${y + 5} V ${y + 10}`);
    li1.setAttributeNS(null, "stroke", "black");
    li1.setAttributeNS(null, "stroke-width", "3");
    group.appendChild(li1);
    const li2 = document.createElementNS(this.svgNS, "path");
    li2.setAttributeNS(null, "d", `M${x} ${y + 5} L ${x + 7} ${y + 10}`);
    li2.setAttributeNS(null, "stroke", "black");
    li2.setAttributeNS(null, "stroke-width", "3");
    group.appendChild(li2);
    const li3 = document.createElementNS(this.svgNS, "path");
    li3.setAttributeNS(null, "d", `M${x} ${y + 5} L ${x - 7} ${y + 10}`);
    li3.setAttributeNS(null, "stroke", "black");
    li3.setAttributeNS(null, "stroke-width", "3");
    group.appendChild(li3);
    const li4 = document.createElementNS(this.svgNS, "path");
    li4.setAttributeNS(null, "d", `M${x} ${y + 10} L ${x + 7} ${y + 20}`);
    li4.setAttributeNS(null, "stroke", "black");
    li4.setAttributeNS(null, "stroke-width", "3");
    group.appendChild(li4);
    const li5 = document.createElementNS(this.svgNS, "path");
    li5.setAttributeNS(null, "d", `M${x} ${y + 10} L${x - 7} ${y + 20}`);
    li5.setAttributeNS(null, "stroke", "black");
    li5.setAttributeNS(null, "stroke-width", "3");
    group.appendChild(li5);

    group.addEventListener("click", e => {
      this.have++;
      this.showScore(this.have, this.max);
      group.remove();
    });

    setTimeout(() => {
      group.remove();
    }, showTime);
  }

  /**
   * Metoda zivotniho cyklu hry
   * @param {*} people pole souradnic, kde se bude zobrazovat hledany objekt
   */
  playGame(people) {
    this.showTime(this.time);

    //v liche sekundy zobrazi hledany objekt
    if (this.time % 2 == 1 && this.peopleIndex < this.max) {
      this.showMe(
        people[this.peopleIndex].x,
        people[this.peopleIndex].y,
        this.show,
      );
      this.peopleIndex++;
    }

    //pokud dojde cas tak se okno zavre
    if (this.time < 0) {
      let reactRoot = document.querySelector("#root");
      reactRoot.classList.remove("onBackSide");
      this.svg.remove();
    }

    //pokud najde vse ukonci hru jinak pokracuje
    if (this.have != this.max) {
      setTimeout(() => {
        this.time--;
        this.playGame(people);
      }, 1000);
    } else {
      this.createInfoText("Gratuluju :)", 280, 140);
    }
  }
}

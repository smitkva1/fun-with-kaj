/**
 * Hierarchicky nejvyse postavena trida reactu
 */
class App extends React.Component {
  constructor(props) {
    super(props);
    this.menu = ["Welcome", "Game", "Profil", "Statistiky"];
    this.state = {
      page: "Welcome",
    };
  }
  /**
   * Stara se o smazani loadingu a nastaveni stranky
   * Nelze zobrazit na prvni strance hned pomocne bocni okno
   */
  componentWillMount() {
    let zeroPage = document.querySelector(".zeroPage");
    zeroPage.remove();

    let found = false;
    let rawPage = window.location.hash;
    if (rawPage !== "") {
      let page = rawPage.slice(1);
      if (page == "help") {
        window.location.href = window.location.pathname;
        this.setState({ page: "Welcome" });
      } else {
        for (let pageName of this.menu) {
          if (page === pageName) {
            this.setState({ page: page });
            found = true;
            break;
          }
        }

        if (!found) {
          this.setState({ page: "Error" });
        }
      }
    } else {
      this.setState({ page: "Welcome" });
    }
  }

  /**
   * Zpravuje zmenu stranky
   */
  componentDidMount() {
    window.addEventListener("hashchange", e => {
      let rawPage = window.location.hash;
      if (rawPage !== "") {
        let page = rawPage.slice(1);
        if (page !== "help") {
          let found = false;
          for (let pageName of this.menu) {
            if (page === pageName) {
              this.setState({ page: page });
              found = true;
              break;
            }
          }
          if (!found) {
            this.setState({ page: "Error" });
          }
        }
      }
    });
  }

  render() {
    return (
      <div className="page-layout">
        <Header
          menu={this.menu}
          changePage={i => this.setState({ page: i })}
          page={this.state.page}
        />
        <Main page={this.state.page} />
        <Aside />
        <Footer />
      </div>
    );
  }
}

//naveseni reactu na DOM
const onePageApp = document.querySelector("#root");
ReactDOM.render(<App />, onePageApp);

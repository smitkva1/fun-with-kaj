/**
 * Trida zobrazujici uvodni stranku aplikace
 * Nachazi se v hierarchii pod tridou Main
 */
class Welcome extends React.Component {
  constructor(props) {
    super(props);
  }

  /**
   * Jen zkryje info bar
   */
  componentWillMount() {
    this.props.infoBar("close", "Chyba");
  }

  render() {
    return (
      <section>
        <section>
          <h1 className="home-title">Do rytmu!</h1>
          <p className="home-motivation">
            Jedná se o jedinečnou příležitost zažít neobyčejný zážitek z
            poslouchání vlastní hudby!
          </p>
          <p className="home-motinvation">
            Posloucháte písničky a máte pocit, že tomu něco chybí, zkuste tohle!
          </p>
          <p className="home-motivation">
            Baví vás vychutnávat si nové možnosti webu, máme pro vás delikatesu!
          </p>
        </section>
      </section>
    );
  }
}

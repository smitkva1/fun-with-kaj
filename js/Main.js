/**
 * Trida zajistujici zobrazujici jednotlivych stranek
 * Nachazi se v hierarchii pod tridou App
 */
class Main extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      infoType: "close",
      infoText: "Chyba",
    };
  }

  /**
   * Nasadi posluchace na online stav
   */
  componentWillMount() {
    window.addEventListener("online", e => {
      console.log("ok", "Super! Internet je zpět.");
    });
    window.addEventListener("offline", e => {
      this.changeInfoBar(
        "error",
        "Pozor právě jste se ocitl offline. Je možné, že aplikace nebude fungovat dle očekávání. ",
      );
    });
  }

  /**
   * Vrati chybovou stranku
   */
  errorPage = () => {
    return (
      <section className="errorPage">
        <h1>Chyba!</h1>
        <p>Tato adresa neexistuje.</p>
      </section>
    );
  };

  /**
   * Zmeni stav info baru
   */
  changeInfoBar = (type, text) => {
    this.setState({
      infoType: type,
      infoText: text,
    });
  };

  render() {
    //nastaveni stylu info baru
    let infoStyle = "infoBar";
    if (this.state.infoType === "close") {
      infoStyle += " " + "infoBarHidden";
    }
    if (this.state.infoType === "ok") {
      infoStyle += " " + "successInfo";
    }
    if (this.state.infoType === "error") {
      infoStyle += " " + "errorInfo";
    }

    //vybrani stranky podle page, ktera je prevzata z App
    let page;
    switch (this.props.page) {
      case "Welcome":
        page = (
          <Welcome infoBar={(type, text) => this.changeInfoBar(type, text)} />
        );
        break;
      case "Game":
        page = (
          <Game infoBar={(type, text) => this.changeInfoBar(type, text)} />
        );
        break;
      case "Profil":
        page = (
          <Profil infoBar={(type, text) => this.changeInfoBar(type, text)} />
        );
        break;
      case "Statistiky":
        page = (
          <Statistics
            infoBar={(type, text) => this.changeInfoBar(type, text)}
          />
        );
        break;
    }

    return (
      <main>
        <div className={infoStyle}>{this.state.infoText}</div>
        {this.props.page === "Error" ? this.errorPage() : page}
      </main>
    );
  }
}

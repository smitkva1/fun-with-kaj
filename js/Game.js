/**
 * Trida zobrazujici stranku samotne hry
 * Nachazi se v hierarchii pod tridou Main
 */
class Game extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      songName: "",
      welcome: true,
      prepared: false,
      endGame: false,
      isWin: false,
      pausa: false,
    };
    this.audioHandler = new AudioHandler();
  }

  /**
   * Jen zkryje info bar
   */
  componentWillMount() {
    this.props.infoBar("close", "Chyba");
  }

  /**
   * Herni smzcka
   * Stara se jak o vykreslocani tak o zvuk
   */
  gameLoop = () => {
    //delka jednoho bloku v sekundach
    const blockDuration = 4;
    //inicializace dulezitych prvku
    const audio = document.querySelector(".gameAudio > audio");
    const blocks = Math.floor(audio.duration / blockDuration) + 1;
    const roadDirectionData = this.audioHandler.executeAudioData(blocks);
    let roadDataPointer = 0;

    //canvas inicialization
    const canvas = document.querySelector(".game");
    const ctx = canvas.getContext("2d");
    let raf;
    //inicializace funkce pro konec hry
    const doneGame = win => this.doneGame(win);

    //inicializace vykreslovacich objektu
    const countryside = new ImgDrawObj(
      0,
      0,
      canvas.width,
      canvas.height,
      "img/countryside.png",
    );

    const road = new RoadObj(
      220,
      99,
      450,
      300,
      {
        x1: 275,
        y1: 99,
        x2: 325,
        y2: 99,
        x3: 415,
        y3: 300,
        x4: 205,
        y4: 300,
        xp1: 240,
        yp1: 200,
        xp2: 360,
        yp2: 200,
      },
      { x1: 70, x2: 530, x3: 415, x4: 205 },
      5,
    );

    const car = new CarImgObj(240, 180, 120, 120, "img/car.png");

    let trees = [];
    const nummberOfTrees = 10;
    const treeSize = 100;
    const treeData = [
      { x: 50, y: 100, dX: 100 },
      { x: 150, y: 100, dX: 200 },
      { x: 450, y: 100, dX: 400 },
      { x: 100, y: 150, dX: 150 },
      { x: 200, y: 150, dX: 250 },
      { x: 400, y: 150, dX: 350 },
      { x: 500, y: 150, dX: 450 },
      { x: 50, y: 200, dX: 100 },
      { x: 150, y: 200, dX: 200 },
      { x: 450, y: 200, dX: 400 },
    ];
    for (let i = 0; i < nummberOfTrees; i++) {
      let tree = new TreeImgObj(
        treeData[i].x,
        treeData[i].y,
        treeSize,
        treeSize,
        Math.floor(Math.random() * 10) % 2 ? "img/tree1.png" : "img/tree2.png",
        treeData[i].x + treeSize / 2 > canvas.width / 2 ? "right" : "left",
        treeData[i].dX,
      );
      trees.push(tree);
    }

    //pomocne promene
    const velocity = 0.5;
    let direction = 0;
    let audioTime;
    let setDirection = false;
    let info;

    //vnitrni funkce zajistujici vzkreslovani canvasu
    const draw = function() {
      const pixels = ctx.getImageData(0, 0, canvas.width, canvas.height);
      ctx.clearRect(0, 0, canvas.width, canvas.height);

      //draw thinks
      countryside.draw(ctx);

      //posun v poli smeru
      audioTime = Math.round(audio.currentTime);
      if (audioTime % blockDuration == 0) {
        if (!setDirection) {
          info = road.getArrays();
          direction = roadDirectionData[roadDataPointer];
          roadDataPointer++;
          setDirection = true;
        }
      } else {
        setDirection = false;
      }

      //vykresleni silnice
      road.move(canvas, direction, velocity);
      road.draw(ctx);
      //vyhresleni auta
      car.draw(ctx);
      //vykresleni stromu
      for (let t of trees) {
        if (t.canDraw(canvas, ctx)) {
          t.draw(canvas, ctx, velocity);
        }
      }

      //ziskani kontrolnich dat z auta
      //182, 255, 0 => light green
      const carData = car.getControlPoints();
      let pixel = ctx.getImageData(carData[0].x, carData[0].y, 1, 1);
      let data = pixel.data;
      const leftOk = data[0] !== 182 && data[1] !== 255 && data[2] !== 0;

      pixel = ctx.getImageData(carData[1].x, carData[1].y, 1, 1);
      data = pixel.data;
      const rightOk = data[0] !== 182 && data[1] !== 255 && data[2] !== 0;

      //podminka konce hry nebo pausa
      if (this.state.isWin) {
        //vypise konec hry
        ctx.fillStyle = "black";
        ctx.font = "60px serif";
        ctx.fillText("Winner!", 170, 100);
        //ukonci animaci
        window.cancelAnimationFrame(raf);
        //udela statistiku
        doneGame(true);
      } else {
        if (leftOk && rightOk) {
          raf = window.requestAnimationFrame(draw);
        } else {
          ctx.fillStyle = "black";
          ctx.font = "60px serif";
          ctx.fillText("Game over", 170, 100);
          //vypne hudbu
          audio.pause();

          //ukonci animaci
          window.cancelAnimationFrame(raf);
          //udela statistiku
          doneGame(false);
        }
      }
    }.bind(this);

    raf = window.requestAnimationFrame(draw);
  };

  /**
   * Postara se o konec hry pro statistiku
   */
  doneGame = isWin => {
    if (isWin) {
      //poresi vzhrane hry
      let winGame = localStorage.getItem("winGame");
      if (winGame === null) {
        localStorage.setItem("winGame", "1");
      } else {
        winGame = JSON.parse(winGame);
        winGame++;
        localStorage.setItem("winGame", winGame);
      }

      //poresi vyhrane pisnicky
      let winSongs = localStorage.getItem("winSongs");
      if (winSongs === null) {
        winSongs = [];
        winSongs.push(this.state.songName);
      } else {
        winSongs = JSON.parse(winSongs);
        if (!winSongs.includes(this.state.songName)) {
          winSongs.push(this.state.songName);
        }
      }
      localStorage.setItem("winSongs", JSON.stringify(winSongs));
    }

    setTimeout(() => {
      this.setState({ endGame: true });
    }, 3000);
  };

  /**
   * Zajistuje odsouhlaseni nahrane pisnicky
   * Pricteni jedne nahrane pisnicky
   */
  handleSong = () => {
    const audio = document.querySelector(".gameAudio");
    if (this.state.songName !== "") {
      audio.classList.remove("unvisibility");

      let uploadSongs = localStorage.getItem("songs");
      if (uploadSongs === null) {
        uploadSongs = [];
      } else {
        uploadSongs = JSON.parse(uploadSongs);
      }

      if (!uploadSongs.includes(this.state.songName)) {
        uploadSongs.push(this.state.songName);
        localStorage.setItem("songs", JSON.stringify(uploadSongs));
      }
      this.setState({ prepared: true, welcome: false });
      this.props.infoBar("ok", "Písnička byla úspěšně nahrána.");
    } else {
      this.props.infoBar(
        "error",
        "Nelze pokračovat dále bez nahrání písničky. Pokud jste písničku nahráli, je třeba zvolit jinou.",
      );
    }
  };

  /**
   * Zpracovani pisnicky z inputu
   * zavedenid at do audioHandleru
   */
  addSongHandler = e => {
    console.log(e.target.files[0]);
    const audio = document.querySelector(".gameAudio > audio");
    const reader = new FileReader();

    reader.addEventListener("load", e => {
      console.log(e);
      audio.src = reader.result;
    });

    reader.readAsDataURL(e.target.files[0]);

    this.audioHandler.setAudioData(e.target.files[0]);

    this.setState({
      songName: e.target.files[0].name,
    });
  };

  /**
   * Pokud chce hrac zvolit jinou pisen
   */
  handleChooseBack = () => {
    //zkryje info bar
    this.props.infoBar("close", "Chyba");
    //nastavi nahravani pisne na false
    this.audioHandler.setSongLoadToFalse();

    const audio = document.querySelector(".gameAudio");
    audio.classList.add("unvisibility");
    this.setState({ prepared: false, welcome: true });
  };

  /**
   * Pokud chce hrac zacit hru
   */
  handleStartGame = () => {
    if (this.audioHandler.isSongLoad()) {
      //zkryje info bar
      this.props.infoBar("close", "Chyba");

      //zapocte zahranou hru
      let playedGame = localStorage.getItem("playedGame");
      if (playedGame === null) {
        playedGame = 0;
      }
      playedGame++;
      localStorage.setItem("playedGame", playedGame);

      //zapne canvas
      this.gameLoop();

      //pusti audio prehravac na strance
      const audio = document.querySelector(".gameAudio > audio");
      audio.currentTime = 0;
      audio.play();

      //funkce nastavujici viteze
      const win = event => {
        this.setState({ isWin: true });
      };

      //naveseni audio posluchace nakonec
      audio.addEventListener("ended", win, false);

      this.setState({ prepared: false, welcome: false });
    } else {
      this.props.infoBar(
        "error",
        "Omlouváme se, nahrávání písně ještě neskončilo, zkuste to za chvíli znovu.",
      );
    }
  };

  /**
   * Obstarajici situaci po hre
   */
  endGamaHandler = () => {
    this.setState({
      prepared: true,
      endGame: false,
      isWin: false,
    });
  };

  render() {
    return (
      <section>
        <h1>Hra</h1>
        <div className="canvasContainer">
          {this.state.prepared && (
            <div className="readyTostart">
              <button onClick={() => this.handleStartGame()}>Začít hru</button>
              <button onClick={() => this.handleChooseBack()}>
                Vybrat novou píseň
              </button>
            </div>
          )}
          {this.state.welcome && (
            <div className="chooseSongBox">
              <form id="addSong" onSubmit={() => this.handleSong()}>
                <input type="file" onChange={e => this.addSongHandler(e)} />
              </form>
              <button form="addSong">Vložit</button>
            </div>
          )}
          {this.state.endGame && (
            <div className="chooseSongBox">
              <button onClick={() => this.endGamaHandler()}>Hrát znovu</button>
            </div>
          )}
          <canvas width="600" height="300" className="game">
            Bohužel váš prohlížeš nepodporuje canvas na kterém je tato hra
            postavena.
          </canvas>
        </div>
        <div className="gameAudio unvisibility">
          <span>Je to ona?</span>
          <audio crossOrigin="anonymous" controls>
            Bohužel váš prohlížeč nepodporuje Audio přehrávač
          </audio>
        </div>
        <ul className="game-help">
          <li>Šipkou doprava zatočíte doprava</li>
          <li>Šipkou doleva zatočíte doleva</li>
        </ul>
      </section>
    );
  }
}

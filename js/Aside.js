/**
 * Pomocne bocni okno
 * Nachazi se v hierarchii pod tridou App
 */
class Aside extends React.Component {
  constructor(props) {
    super(props);
  }

  /**
   * Zavre okno pomoci kroku zpet v historii
   */
  closeHandler = () => {
    window.history.back();
  };

  render() {
    return (
      <aside id="help">
        <article>
          <header>Help Desk</header>
          <div className="close" onClick={() => this.closeHandler()}>
            {" "}
            &times;{" "}
          </div>
          <section>
            <h1 className="help-title">Welcome</h1>
            <p className="help-text">
              Zde naleznete úvodní stánku aplikace. Je to první stránka, kterou
              uvidíte po načtení stránky v prohlížeči. Na začátek je zde popáno
              v krátkosi o co se jedná. Dále máte hlavní hezní panel. Po kliknut
              na logo se znovu objevi.
            </p>
            <p>Pozn.: Hra byla vytvořena ve FireFoxu</p>
          </section>
          <section>
            <h1 className="help-title">Hra</h1>
            <p className="help-text">
              Hra se ovládá šipkami. Interakce se stánkou je možná pomocí šipek.
              Pozn.: Cesta je generována přibližně podle písničky. Neočekávejte
              doslovné kopírování.
            </p>
          </section>
          <section>
            <h1 className="help-title">Profil</h1>
            <p className="hepl-text">
              Zde si můžete prohlédnout své data, které o vás uchováváme a též
              je bez omezení můžete měnit. Pouze není umožněno nechat políčko
              Jméno a Email volné. Fotka se mění pomocí natahnutím požadované
              fotky na dané místo. Data se ukládají v LocalStorage, tedy
              nemusíte se o ně bát. Pro ukládání obrázku používáme IndexDB.
            </p>
          </section>
          <section>
            <h1 className="help-title">Statistiky</h1>
            <p className="help-text">
              Na této stránce jsou schrnuty všechny vaše úspěchy i neúspěchy,
              které jste v této hře dosáhli. Veškerá data se ukládají v
              LocalStorage. Nelze je měnit. Data je možné pomocí tlačítka
              "Uložit" uložit do svého počítače.
            </p>
          </section>
          <section>
            <h1 className="help-title">EasterEgg</h1>
            <p className="help-text">
              Klikněte na "U" v logu a buďte ve střehu.
            </p>
          </section>
        </article>
      </aside>
    );
  }
}

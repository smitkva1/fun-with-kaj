/**
 * Hlavicka cele aplikace.
 * Obsahuje logo, menu a proklik na pomocne bocni okno
 * Nachazi se v hierarchii pod tridou App
 */
class Header extends React.Component {
  constructor(props) {
    super(props);
    this.menu = this.props.menu;
  }

  /**
   * Zmeni url api
   */
  butHandler = where => {
    window.location.href = window.location.pathname + "#" + where;
  };

  /**
   * Nasazeni EasterEggu na pismeno U
   */
  componentDidMount() {
    const easterEggGate = document.querySelector("#easterEgg");
    easterEggGate.addEventListener("click", e => {
      if (this.props.page !== "Game") {
        let reactRoot = document.querySelector("#root");
        reactRoot.classList.add("onBackSide");

        new EasterEgg();
      }
    });
  }

  render() {
    return (
      <header>
        <svg
          className="icon"
          width="200"
          height="50"
          viewBox="0 0 400 200"
          xmlns="http://www.w3.org/2000/svg"
          onClick={() => {
            this.props.changePage("Welcome");
            this.butHandler("Welcome");
          }}
        >
          {/*Created with Method Draw - http://github.com/duopixel/Method-Draw/*/}
          <defs>
            <filter
              height="200%"
              width="200%"
              y="-50%"
              x="-50%"
              id="svg_4_blur"
            >
              <feGaussianBlur stdDeviation="2" in="SourceGraphic" />
            </filter>
          </defs>
          <g>
            <title>Layer 1</title>
            <g className="letterShadows">
              <ellipse
                filter="url(#svg_4_blur)"
                ry="11"
                rx="56"
                cy="155.4375"
                cx="88.5"
                fillOpacity="null"
                strokeOpacity="null"
                strokeWidth="0"
                stroke="#000"
                fill="#dedede"
              />
              <ellipse
                filter="url(#svg_4_blur)"
                ry="11"
                rx="56"
                cy="155.4375"
                cx="188.5"
                fillOpacity="null"
                strokeOpacity="null"
                strokeWidth="0"
                stroke="#000"
                fill="#dedede"
              />
              <ellipse
                filter="url(#svg_4_blur)"
                ry="11"
                rx="56"
                cy="155.4375"
                cx="289.5"
                fillOpacity="null"
                strokeOpacity="null"
                strokeWidth="0"
                stroke="#000"
                fill="#dedede"
              />
            </g>
            <g className="iconText">
              <text
                fontWeight="bold"
                xmlSpace="preserve"
                textAnchor="start"
                fontFamily="'Palatino Linotype', 'Book Antiqua', Palatino, serif"
                fontSize="141"
                y="153"
                x="47.125"
                fillOpacity="null"
                strokeOpacity="null"
                strokeWidth="0"
                stroke="#000"
                fill="#000000"
              >
                F
              </text>
              <text
                id="easterEgg"
                fontWeight="bold"
                xmlSpace="preserve"
                textAnchor="start"
                fontFamily="'Palatino Linotype', 'Book Antiqua', Palatino, serif"
                fontSize="141"
                y="153"
                x="125.5"
                fillOpacity="null"
                strokeOpacity="null"
                strokeWidth="0"
                stroke="#000"
                fill="#000000"
              >
                U
              </text>
              <text
                fontWeight="bold"
                xmlSpace="preserve"
                textAnchor="start"
                fontFamily="'Palatino Linotype', 'Book Antiqua', Palatino, serif"
                fontSize="141"
                y="153"
                x="230.5"
                fillOpacity="null"
                strokeOpacity="null"
                strokeWidth="0"
                stroke="#000"
                fill="#000000"
              >
                N
              </text>
            </g>
          </g>
        </svg>
        <nav>
          {this.menu.map((value, index) => {
            if (value !== "Welcome") {
              return (
                <div
                  key={index}
                  onClick={e => {
                    if (this.props.page === "Game") {
                      let audioBox = document.querySelector(".gameAudio");
                      console.log(audioBox.classList.contains("unvisibility"));
                      if (!audioBox.classList.contains("unvisibility")) {
                        let audio = document.querySelector(
                          ".gameAudio > audio",
                        );
                        audio.pause();
                        if (
                          window.confirm(
                            "Opravdu chcete odejít? Nahraná píseň se ztratí a rozehraná hra se započítá jako prohraná.",
                          )
                        ) {
                          window.cancelAnimationFrame(raf);
                          this.props.changePage(e.target.innerText);
                          this.butHandler(e.target.innerText);
                        } else {
                          let preStart = document.querySelector(
                            ".readyTostart",
                          );
                          if (!preStart) {
                            audio.play();
                          }
                        }
                      } else {
                        this.props.changePage(e.target.innerText);
                        this.butHandler(e.target.innerText);
                      }
                    } else {
                      this.props.changePage(e.target.innerText);
                      this.butHandler(e.target.innerText);
                    }
                  }}
                  className={this.props.page === value ? "actualButton" : ""}
                >
                  <p>{value}</p>
                </div>
              );
            }
          })}
          <div onClick={() => this.butHandler("help")}>
            <p>Help</p>
          </div>
        </nav>
      </header>
    );
  }
}

//Gougle Closure
const inheritence = function(child, parent) {
  //F je docasny konstruktor
  const F = function() {};
  F.prototype = parent.prototype;
  child.prototype = new F();
  //konvence pro volani prepsanych metod
  child._superClass = parent.prototype;
  //dobry zvyk aby instance ukazovala na svuj konstruktor
  child._superClass.constructor = child;
};

/**
 * Obecny bjekt
 * @param {*} x x-ova souradnice
 * @param {*} y y-ova souradnice
 * @param {*} width sirka objektu
 * @param {*} height vyska objektu
 */
const DrawObj = function(x, y, width, height) {
  this.x = x;
  this.y = y;
  this.width = width;
  this.height = height;
};
/**
 * Metoda vykresleni obecneho objektu
 */
DrawObj.prototype.draw = function(ctx) {
  ctx.fillRect(this.x, this.y, this.width, this.height);
};

/**
 * Obecny objekt obrazku
 * @param {*} x x-ova souradnice
 * @param {*} y y-ova souradnice
 * @param {*} width sirka objektu
 * @param {*} height vyska objektu
 * @param {*} src cesta k obrazku
 */
const ImgDrawObj = function(x, y, width, height, src) {
  DrawObj.call(this, x, y, width, height);
  this.image = new Image();
  this.image.src = src;
};

//objekt obrazek dedi z obecneho objektu
inheritence(ImgDrawObj, DrawObj);

/**
 * Metoda na vykresleni obrazku
 */
DrawObj.prototype.draw = function(ctx) {
  ctx.drawImage(this.image, this.x, this.y, this.width, this.height);
};

/**
 * Objekt strom
 * @param {*} x x-ova souradnice
 * @param {*} y y-ova souradnice
 * @param {*} width sirka objektu
 * @param {*} height vyska objektu
 * @param {*} src cesta k obrazku
 * @param {*} type na jake strane cesty bude
 * @param {*} dX vychozi x-ova souradnice
 */
const TreeImgObj = function(x, y, width, height, src, type, dX) {
  ImgDrawObj.call(this, x, y, width, height, src);
  this.type = type;
  this.dX = dX;
};

//objekt strom dedi z objektu obrazku
inheritence(TreeImgObj, ImgDrawObj);

/**
 * Metoda vykreslujici strom
 */
TreeImgObj.prototype.draw = function(canvas, ctx, velocity) {
  if (this.y + this.height < canvas.height) {
    if (this.type == "left") {
      this.x -= velocity + 2;
    } else {
      this.x += velocity;
    }

    this.height += velocity * 4;
    this.width += velocity + 2;
    this.y += velocity;
    ctx.drawImage(this.image, this.x, this.y, this.width, this.height);
  } else {
    this.x = this.dX;

    this.y = 90;
    this.width = 0;
    this.height = 0;
  }
};

/**
 * Metoda zjisti zda nema strom v ceste cestu
 * Tak vrati falce, jinak true
 */
TreeImgObj.prototype.canDraw = function(canvas, ctx) {
  let pixel = ctx.getImageData(
    this.x + Math.floor(this.width / 2),
    this.y,
    1,
    canvas.width - this.y,
  );
  let data = pixel.data;
  for (let i = 0; i < data.length; i += 4) {
    //128, 128, 128 => gray
    if (data[i] === 128 && data[i + 1] === 128 && data[i + 2] === 128) {
      return false;
    }
  }
  return true;
};

/**
 * Objekt auta
 * @param {*} x x-ova souradnice
 * @param {*} y y-ova souradnice
 * @param {*} width sirka objektu
 * @param {*} height vyska objektu
 * @param {*} src cesta k obrazku
 */
const CarImgObj = function(x, y, width, height, src) {
  ImgDrawObj.call(this, x, y, width, height, src);

  document.addEventListener(
    "keydown",
    function(e) {
      //console.log(e);
      if (e.key == "ArrowLeft") {
        //console.log("ArrowLeft");
        this.x -= 5;
      }
      if (e.key == "ArrowRight") {
        //console.log("ArrowRight");
        this.x += 5;
      }
    }.bind(this),
  );
};

//objekt auta dedi z objektu obrazku
inheritence(CarImgObj, ImgDrawObj);

/**
 * Metoda vrati kontrolni body na aute v poli
 * i = 0 => levy
 * i = 1 => pravy
 */
CarImgObj.prototype.getControlPoints = function() {
  let controlY = this.y + Math.floor((this.height * 2) / 3);

  return [{ x: this.x, y: controlY }, { x: this.x + this.width, y: controlY }];
};

/**
 * Objekt silnice
 * @param {*} x x-ova souradnice
 * @param {*} y y-ova souradnice
 * @param {*} width sirka objektu
 * @param {*} height vyska objektu
 * @param {*} points bocni body silnice + dva pomocne
 * @param {*} breakPoints hranicni body ya ktere jiz silnice nesmi
 * @param {*} gap mezera mezi krajni carou a koncem silnice
 */
const RoadObj = function(x, y, width, height, points, breakPoints, gap) {
  DrawObj.call(this, x, y, width, height);
  this.points = points;
  this.breakPoints = breakPoints;
  this.gap = gap;

  this.lastLeftPosition = 600;
  this.lastRightPosition = 0;
  this.maxLeft = [];
  this.maxRight = [];
  this.backDirection = 0;

  this.backStart = false;
};

//objekt silnice dedi z obecneho objektu
inheritence(RoadObj, DrawObj);

/**
 * Metoda vykreslujici silnici
 */
RoadObj.prototype.draw = function(ctx) {
  ctx.beginPath();
  ctx.setLineDash([]);
  ctx.moveTo(this.points.x1, this.points.y1);
  ctx.lineTo(this.points.x2, this.points.y2);
  ctx.quadraticCurveTo(
    this.points.xp2,
    this.points.yp2,
    this.points.x3,
    this.points.y3,
  );
  ctx.lineTo(this.points.x4, this.points.y4);
  ctx.quadraticCurveTo(
    this.points.xp1,
    this.points.yp1,
    this.points.x1,
    this.points.y1,
  );
  ctx.closePath();
  ctx.fillStyle = "gray";
  ctx.fill();

  //bocni cary
  ctx.beginPath();
  ctx.moveTo(this.points.x2 - this.gap, this.points.y2);
  ctx.quadraticCurveTo(
    this.points.xp2 - this.gap,
    this.points.yp2,
    this.points.x3 - this.gap,
    this.points.y3,
  );
  ctx.moveTo(this.points.x4 + this.gap, this.points.y4);
  ctx.quadraticCurveTo(
    this.points.xp1 + this.gap,
    this.points.yp1,
    this.points.x1 + this.gap,
    this.points.y1,
  );
  ctx.strokeStyle = "white";
  ctx.stroke();
  ctx.closePath();

  //stredova cara
  ctx.beginPath();
  ctx.setLineDash([10, 10]);
  ctx.moveTo(
    Math.floor((this.points.x2 - this.points.x1) / 2) + this.points.x1,
    this.points.y2,
  );
  ctx.quadraticCurveTo(
    Math.floor((this.points.xp2 - this.points.xp1) / 2) + this.points.xp1,
    this.points.yp2,
    Math.floor((this.points.x3 - this.points.x4) / 2) + this.points.x4,
    this.points.y3,
  );
  ctx.strokeStyle = "white";
  ctx.stroke();
  ctx.closePath();
};

/**
 * Metoda posouvajici silnici
 */
RoadObj.prototype.move = function(canvas, direction, velocity) {
  //ovladani zadpredni casti silnice
  switch (direction) {
    case -1:
      if (this.points.x1 > this.breakPoints.x1) {
        this.points.x1 -= velocity;
        this.points.x2 -= velocity;

        //nastaveni hranice napravo
        if (this.lastRightPosition > this.points.x2) {
          this.maxRight.push(this.lastRightPosition - velocity);
          this.lastRightPosition = 0;
        }
        this.lastLeftPosition = this.points.x1;

        //ovladani pomocnych bodu
        this.points.xp1 -= velocity;
        this.points.xp2 -= velocity;
      } else {
        direction = 0;
      }
      break;
    case 1:
      if (this.points.x2 < this.breakPoints.x2) {
        this.points.x1 += velocity;
        this.points.x2 += velocity;

        //nastaveni hranice nalevo
        if (this.lastLeftPosition < this.points.x1) {
          this.maxLeft.push(this.lastLeftPosition + velocity);
          this.lastLeftPosition = 600;
        }
        this.lastRightPosition = this.points.x2;

        //ovladani pomocnych bodu
        this.points.xp1 += velocity;
        this.points.xp2 += velocity;
      } else {
        direction = 0;
      }
      break;
  }

  //aby nabral zadek zpozdeni
  if (
    this.maxLeft.length > 0 ||
    this.maxRight.length > 0 ||
    this.points.x1 < this.points.x4 ||
    this.points.x2 > this.points.x3
  ) {
    this.backStart = true;
  }

  //vypocet smeru zadku
  if (this.backStart) {
    //70 je rozdil mezi zacatecnima hodnotama x1 a x4
    let leftRecount = this.points.x4 + 70;
    let rightRecount = this.points.x3 - 70;

    //console.log(leftRecount, this.maxLeft, rightRecount, this.maxRight);

    if (this.maxLeft[0] >= leftRecount) {
      this.maxLeft = this.maxLeft.slice(1);
      if (this.maxRight.length > 0) {
        this.backDirection = 1;
      } else {
        this.backDirection = 0;
        this.backStart = false;
      }
    } else if (this.maxRight[0] <= rightRecount) {
      this.maxRight = this.maxRight.slice(1);
      if (this.maxLeft.length > 0) {
        this.backDirection = -1;
      } else {
        this.backDirection = 0;
        this.backStart = false;
      }
    } else if (
      this.maxLeft.length == 0 &&
      this.maxRight.length > 0 &&
      this.maxRight[0] > rightRecount
    ) {
      this.backDirection = 1;
    } else if (
      this.maxRight.length == 0 &&
      this.maxLeft.length > 0 &&
      this.maxLeft[0] < leftRecount
    ) {
      this.backDirection = -1;
    } else if (this.maxLeft.length == 0 && this.maxRight.length == 0) {
      if (this.points.x4 == this.breakPoints.x4 || direction != 0) {
        this.backDirection = direction;
      }
    }
    //nemenit: if (this.backDirection == -1 && this.maxLeft[0] < leftRecount)
    //nemenit: if (this.backDirection == 1 && this.maxRight[0] > rightRecount)

    //pokud se smer zastavi je potreba udelat opet zpozdeni
    if (this.backDirection == 0 && direction == 0) {
      this.backStart = false;
    }
  }

  //ovladani zadni casti silnice
  switch (this.backDirection) {
    case -1:
      if (this.points.x4 > 0) {
        this.points.x3 -= velocity;
        this.points.x4 -= velocity;
      } else {
        this.backDirection = 0;
        this.backStart = false;
      }
      break;
    case 1:
      if (this.points.x3 < canvas.width) {
        this.points.x3 += velocity;
        this.points.x4 += velocity;
      } else {
        this.backDirection = 0;
        this.backStart = false;
      }
      break;
  }
};
RoadObj.prototype.getArrays = function() {
  return [this.maxLeft, this.maxRight];
};

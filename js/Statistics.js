/**
 * Trida zobrazujici statistiku hrace
 * Nachazi se v hierarchii pod tridou Main
 */
class Statistics extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      playedGame: "",
      winGame: "",
      uploadSongs: "",
      winSongs: "",
      skills: "",
    };
    this.downloadData = this.downloadData.bind(this);
  }

  /**
   * Nahrani dat z LocalStorage a pripadne zalozeni zaznamu
   */
  componentWillMount() {
    //zkryje info bar
    this.props.infoBar("close", "Chyba");

    let playedGame = localStorage.getItem("playedGame");
    let winGame = localStorage.getItem("winGame");
    let uploadSongs = localStorage.getItem("songs");
    let winSongs = localStorage.getItem("winSongs");
    let skills = 0;

    if (playedGame === null) {
      playedGame = 0;
      localStorage.setItem("playedGame", "0");
    }
    if (winGame === null) {
      winGame = 0;
      localStorage.setItem("winGame", "0");
    }
    if (uploadSongs === null) {
      uploadSongs = [];
      localStorage.setItem("songs", JSON.stringify(uploadSongs));
    } else {
      uploadSongs = JSON.parse(uploadSongs);
    }
    if (winSongs === null) {
      winSongs = [];
      localStorage.setItem("winSongs", JSON.stringify(winSongs));
    } else {
      winSongs = JSON.parse(winSongs);
    }

    console.log(winSongs.length);
    uploadSongs = uploadSongs.length;
    winSongs = winSongs.length;
    if (
      playedGame &&
      winGame &&
      uploadSongs &&
      winSongs &&
      playedGame !== "0" &&
      uploadSongs !== "0"
    ) {
      skills = ((winGame / playedGame + winSongs / uploadSongs) / 2) * 100;
    }

    this.setState({
      playedGame: playedGame,
      winGame: winGame,
      uploadSongs: uploadSongs,
      winSongs: winSongs,
      skills: skills,
    });
  }

  /**
   * Metoda zpracovava pozadavek na stazeni dat
   * Pripravi objekt na export
   */
  downloadData() {
    const name = localStorage.getItem("name");
    const email = localStorage.getItem("email");

    if (name !== null && name !== "" && email !== null && email !== "") {
      const age = localStorage.getItem("age");
      const tel = localStorage.getItem("tel");
      const songs = JSON.parse(localStorage.getItem("songs"));

      let myData = {
        name: name,
        age: age,
        email: email,
        tel: tel,
        songs: songs,
        playedGame: this.state.playedGame,
        winGame: this.state.winGame,
        uploadSongs: this.state.uploadSongs,
        winSongs: this.state.winSongs,
        skills: this.state.skills,
      };

      this.downloadString(JSON.stringify(myData), "text/json", "myData.json");
    } else {
      this.props.infoBar(
        "error",
        "Nejdříve je třeba vyplnit alespoň povinná data z profilu.",
      );
    }
  }

  /**
   * Metoda zajistujici stazeni dat pres BLOB a html znacku <a>
   */
  downloadString = (text, fileType, fileName) => {
    let blob = new Blob([text], { type: fileType });

    let a = document.createElement("a");
    a.download = fileName;
    a.href = URL.createObjectURL(blob);
    a.dataset.downloadurl = [fileType, a.download, a.href].join(":");
    a.style.display = "none";
    document.body.appendChild(a);
    a.click();
    document.body.removeChild(a);
    setTimeout(function() {
      URL.revokeObjectURL(a.href);
    }, 1500);
  };

  /**
   * Smaze statistiku
   */
  deleteAll = () => {
    this.setState({
      playedGame: 0,
      winGame: 0,
      uploadSongs: 0,
      winSongs: 0,
      skills: 0,
    });

    localStorage.setItem("playedGame", "0");
    localStorage.setItem("winGame", "0");
    localStorage.setItem("songs", "[]");
    localStorage.setItem("winSongs", "[]");

    this.props.infoBar("ok", "Statistika byla smazána.");
  };

  render() {
    return (
      <section>
        <h1>Statistiky</h1>
        <table>
          <tbody>
            <tr>
              <td>Odehraných her:</td>
              <td>{this.state.playedGame}</td>
            </tr>
            <tr>
              <td>Vyhraných her:</td>
              <td>{this.state.winGame}</td>
            </tr>
            <tr>
              <td>Nahraných písniček</td>
              <td>{this.state.uploadSongs}</td>
            </tr>
            <tr>
              <td>Vyhraných písniček</td>
              <td>{this.state.winSongs}</td>
            </tr>
            <tr>
              <td>Skills</td>
              <td>
                <progress max="100" value={this.state.skills} />
              </td>
            </tr>
          </tbody>
        </table>
        <div className="buttomsGroup">
          <button className="removeData" onClick={this.deleteAll}>
            Smazat statistiku
          </button>
          <button onClick={this.downloadData}>Stáhnout vlastní data</button>
        </div>
      </section>
    );
  }
}

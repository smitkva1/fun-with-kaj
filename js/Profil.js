/**
 * Trida zobrazujici profil hrace
 * Nachazi se v hierarchii pod tridou Main
 */
class Profil extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      image: "",
      name: "",
      age: "",
      tel: "",
      email: "",
      songs: [],
    };
  }

  /**
   * Nahraje data z localStorage, pokud nejsou, tak je zalozi
   */
  componentWillMount() {
    //zkryje info bar
    this.props.infoBar("close", "Chyba");

    let name = localStorage.getItem("name");
    let age = localStorage.getItem("age");
    let email = localStorage.getItem("email");
    let tel = localStorage.getItem("tel");
    let songs = JSON.parse(localStorage.getItem("songs"));

    if (name === null) {
      name = "";
      localStorage.setItem("name", "");
    }
    if (age === null) {
      age = "";
      localStorage.setItem("age", "");
    }
    if (email === null) {
      email = "";
      localStorage.setItem("email", "");
    }
    if (tel === null) {
      tel = "";
      localStorage.setItem("tel", "");
    }
    if (songs === null) {
      songs = [];
      localStorage.setItem("songs", JSON.stringify(songs));
    }

    this.setState({
      name: name,
      age: age,
      email: email,
      tel: tel,
      songs: songs,
    });
  }

  /**
   * Logika avataru
   * Nasazeni logiky Drag and drop a prace s IndexDB
   */
  componentDidMount() {
    const dndContainer = document.querySelector(".avatarContainer");
    const avaterImg = document.querySelector(".avatar");

    dndContainer.addEventListener("drop", e => this.showImg(e));
    dndContainer.addEventListener("dragover", e => e.preventDefault());

    var request = indexedDB.open("mojeDb", 1);

    request.onerror = function(e) {
      alert("Není dovolený přístup k IndexDB!?");
    };

    request.onupgradeneeded = function(e) {
      let db = e.target.result;
      let r = db.createObjectStore("tabulka", { autoIncrement: true });
    };

    request.onsuccess = function(e) {
      console.log("Success creating/accessing IndexedDB database");
      let db = e.target.result;
      let t = db.transaction(["tabulka"], "readwrite");
      let r = t.objectStore("tabulka").getAll();

      r.onsuccess = function(e) {
        const num = e.target.result.length;
        //console.log(e.target.result[num - 1]);
        if (e.target.result[num - 1] !== undefined) {
          avaterImg.src = e.target.result[num - 1];
        } else {
          avaterImg.src = "img/empty.png";
        }
      };
    };
  }

  /**
   * Metoda pro zobrazovani avatara z Drag and drop i y IndexDB
   */
  showImg = e => {
    const avaterImg = document.querySelector(".avatar");

    e.preventDefault();
    const files = e.dataTransfer.files;
    const f = files[0];

    if (f.type.indexOf("image" != -1)) {
      const fr = new FileReader();
      fr.addEventListener("load", e => {
        avaterImg.src = fr.result;

        var request = indexedDB.open("mojeDb", 1);

        request.onerror = function(e) {
          alert("Není dovolený přístup k IndexDB!?");
        };

        request.onupgradeneeded = function(e) {
          let db = e.target.result;
          let r = db.createObjectStore("tabulka", { autoIncrement: true });
        };

        request.onsuccess = function(e) {
          console.log("Success creating/accessing IndexedDB database");
          let db = e.target.result;
          let t = db.transaction(["tabulka"], "readwrite");
          //let r = t.objectStore("tabulka").clear();
          let r = t.objectStore("tabulka").add(fr.result);

          /*r.onsuccess = function(e) {
            let db = e.target.result;
            let t = db.transaction(["tabulka"], "readwrite");
          };*/
        };
      });
      fr.readAsDataURL(f);
    }
  };

  /**
   * Metoda zachycujici zmeny formulare
   */
  handleInput = e => {
    switch (e.target.id) {
      case "formName":
        this.setState({ name: e.target.value });
        break;
      case "formAge":
        this.setState({ age: e.target.value });
        break;
      case "formEmail":
        this.setState({ email: e.target.value });
        break;
      case "formTel":
        this.setState({ tel: e.target.value });
        break;
    }
  };

  /**
   * Ulozeni formulare
   */
  handleForm = () => {
    localStorage.setItem("name", this.state.name);
    localStorage.setItem("age", this.state.age);
    localStorage.setItem("email", this.state.email);
    localStorage.setItem("tel", this.state.tel);

    this.props.infoBar("ok", "Data byla uložena.");
  };

  /**
   * Smazani formulare
   */
  deleteAll = () => {
    this.setState({
      name: "",
      age: "",
      email: "",
      tel: "",
      songs: [],
    });
    localStorage.setItem("songs", "[]");
    localStorage.setItem("name", "");
    localStorage.setItem("age", "");
    localStorage.setItem("email", "");
    localStorage.setItem("tel", "");

    this.props.infoBar("ok", "Data profilu byla smazána.");
  };

  render() {
    return (
      <section>
        <h1>Profil</h1>
        <div>
          <div className="avatarContainer">
            <img
              className="avatar"
              type="image"
              src=""
              alt="avatar"
              width="100"
              height="100"
            />
          </div>
        </div>
        <form id="userInfo" onSubmit={() => this.handleForm()}>
          <label>Jméno</label>
          <input
            id="formName"
            type="text"
            placeholder="Váše jméno a příjmení"
            pattern="^([A-ZÁ-Ž]{1}[a-zá-ž]+)(\s{1})([A-ZÁ-Ž]{1}[a-zá-ž]+\s{1})*([A-ZÁ-Ž]{1}[a-zá-ž]+)$"
            title="Jméno musí být ve formátu: Jméno Přijmení"
            value={this.state.name}
            onChange={e => this.handleInput(e)}
            required
          />
          <label>Věk</label>
          <input
            id="formAge"
            type="number"
            placeholder="Váš věk"
            value={this.state.age}
            min="1"
            max="120"
            onChange={e => this.handleInput(e)}
          />
          <label>Email</label>
          <input
            id="formEmail"
            type="email"
            placeholder="Váš email"
            value={this.state.email}
            onChange={e => this.handleInput(e)}
            required
          />
          <label>Telefon</label>
          <input
            id="formTel"
            type="tel"
            placeholder="Váš telefon"
            pattern="^([\d]{3})(\s{1})(\d{3})(\s{1})(\d{3})$"
            title="Telefon musí být ve formátu: 777 777 777"
            value={this.state.tel}
            onChange={e => this.handleInput(e)}
          />
          <label>Nahrané písničky</label>
          {this.state.songs.length > 0 ? (
            this.state.songs.map((elem, key) => {
              return <span key={key}>{elem}</span>;
            })
          ) : (
            <span>--žádné--</span>
          )}
        </form>
        <div className="buttomsGroup">
          <button className="removeData" onClick={() => this.deleteAll()}>
            Smazat
          </button>
          <button form="userInfo" type="submit">
            Ulozit
          </button>
        </div>
      </section>
    );
  }
}

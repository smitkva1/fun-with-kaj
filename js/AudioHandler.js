/**
 * Metoda zpracovavajici zvuk pro hru
 * Nachazi se bokem od Reactu
 */
class AudioHandler {
  constructor() {
    this.load = false;
    this.AudioContext = window.AudioContext || window.webkitAudioContext;
    this.audioCtx = new AudioContext();
    this.currentBuffer = null;
    this.audioElement = null;
    this.audioArray = null;
  }

  /**
   * Vrati hodnotu, zda je pisen nahrana
   */
  isSongLoad() {
    return this.load;
  }

  /**
   * Pri nahravani dalsich pisni je treba nastavit na false
   */
  setSongLoadToFalse() {
    this.load = false;
  }

  /**
   * Metoda vytvori ArrayBuffer
   * @param {*} data ziskane data z <input type="file"
   */
  setAudioData(data) {
    this.audioElement = document.querySelector(".gameAudio > audio");

    let reader = new FileReader();

    reader.addEventListener("load", e => {
      this.audioArray = this.audioCtx.decodeAudioData(
        reader.result,
        function(buffer) {
          this.currentBuffer = buffer;
          this.load = true;
        }.bind(this),
      );
    });

    reader.readAsArrayBuffer(data);
  }

  /**
   * Metoda zpracuje ArrayBuffer a vyselektuje z nej potrebna data v podobe smeru
   * smer se urcuje podle kladnosti hodnoty
   * -1 => do leva
   * 0  => nic
   * 1 => doprava
   * return pole smeru
   */
  executeAudioData(blocks) {
    const totalLength = this.currentBuffer.getChannelData(0).length;
    const blockSize = Math.floor(totalLength / blocks);

    let newData = [];
    for (let i = 0; i < blocks; i++) {
      let audioBufferKey = Math.floor(blockSize * i);
      let specificData = this.currentBuffer.getChannelData(0)[audioBufferKey];

      if (specificData == 0) {
        newData.push(0);
      } else if (specificData < 0) {
        newData.push(-1);
      } else if (specificData > 0) {
        newData.push(1);
      }
    }

    return newData;
  }
}

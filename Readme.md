# Have fun with KAJ
Jedná se o semestrální projekt k předmětu KAJ. Cílem projektu je především prakticky si vyzkoušet maximální počet technologií, které umožní časová kapacita.
Webová aplikace je založena na html, css a js. Dále je použit framework React pro pohodlnější práci při vytváření OnePage aplikace.

Hlavním motivem je jednoduchá hra, která se nachází na stránce Hra. Tato hra je neobyčená způsobem generování smeru cesty. To se odvyjí od písničky, která byla hráčem nahrána. Ta samá písnička je též pouštěna v době hry. Dále je hra ovládána šipkami.

## Postup
Díky jednoduché modularizaci Reactu jsem aplikaci vytvářel po částech. Nejdříve jednoduché React componenty mezi kterými šlo přecházet. Postupně jsem dotvářet styly jednotluvých stránek. Ve chvíli, kdyby všechny componenty vytvořeny a nastylovány, tak jsem přesěl k navazování js api na vytvořenou kostru. Začal jsem History API, pro jednodušší přecházení mezi stránkami, a nakonec jsem naimplementoval ovládání canvasu s Audio API.

## Popis funkčnosti
Aplikace je rozdělena do několika stranek, kde se jsou různé funkční prvky.

### Obecné
- History API pomáhá s přechodem mezi stránkami, z historie se maže informace o navstiveni pomocného bočniho okna
- Responsivní vzhled pomocí media queries
- Loading aplikace
- Pokud chyba při načtení Reactu, tak chybová stránka
- Pokud zadání chybného hashe, tak chybová stránka
- Navěšené posluchače na online/offline událost

### Hra
- File Api na nahráni hudby
- Canvas a operace nad ním
- Pro canvas vykreslování použita prototypová dědičnost
- Audio API pro analýzu hudby
- Html znacka audio

### Profil
- Validace formuláře
- Drag and drop fotky
- IndexDB na uložení fotky
- LocalStorage na uložení dat o hráči

### Statistika
- Stažení dat pomocí BLOB

### EasterEgg
- JS interakce s SVG

/**
 * Pokud se nezobrazi do 5s react tak zobraz chybove hlaseni
 */
let zeroPage = document.querySelector(".zeroPage");

setTimeout(() => {
  if (zeroPage !== null) {
    zeroPage.innerHTML = `<h1>Omlouváme se za komplikace</h1>
      <p>Tato stránka je vytvořena pomocí JS frameworku React.</p>
      <p>Pokud se vám tento text zobrazil, znamená to, že prohlížeč se brání proti Cross origin requestu ${
        !navigator.onLine ? "ne jen jste bez internetu" : ""
      }.</p>`;
  }
}, 5000);
